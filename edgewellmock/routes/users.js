var express = require('express');
var fs = require('fs');
var router = express.Router();

/* GET users listing. */
router.post('/', function(req, res, next) {
	console.log(req.body);
    res.json({msg:"success"});
});
router.get('/search', function(req, res, next) {

	var teams = {
		"suggestions":[
	        {value:'Hydro in mens razors',url:'dfdfdfdf',data:{category:'BRAND'}},
	        {value:'Hydro in mens shave prep',url:'dfdfdfdf',data:{category:'BRAND'}},
	        {value:'Hydro in mens brands',url:'dfdfdfdf',data:{category:'BRAND'}},
	        {value:'Hydro Silk in womens razors & blades',url:'dfdfdfdf',data:{category:'BRAND'}},
	        {value:'Hydro Silk in womens brands',url:'dfdfdfdf',data:{category:'BRAND'}},
	        {value:'MEN Hydro 5Kit',url:'dfdfdfdf',data:{category:'MEN'}},
	        {value:'MEN Hydro 5Razor',url:'dfdfdfdf',data:{category:'MEN'}},
	        {value:'MEN Hydro 5 Refills',url:'dfdfdfdf',data:{category:'MEN'}},
	        {value:'MEN Hydro 5 Sensitive Razor',url:'dfdfdfdf',data:{category:'MEN'}},
	        {value:'WOMEN Hydro Silk Kit',url:'dfdfdfdf',data:{category:'WOMEN'}},
	        {value:'WOMEN Hydro Silk TrimStyle Razor',url:'dfdfdfdf',data:{category:'WOMEN'}},
	        {value:'WOMEN Hydro Silk Razor',url:'dfdfdfdf',data:{category:'WOMEN'}},
	        {value:'WOMEN Hydro Silk Shower Hanging Reills',url:'dfdfdfdf',data:{category:'WOMEN'}},
	    ]
	}


    res.json(teams);
});

router.loadProductJson = function(cb){
    fs.readFile("public/products/oobsearch.json",'utf8', function (err, data) {
        if(!err){
            var data = JSON.parse(data);
            cb(data);
        } 
               
  });
}

router.get('/search-results', function(req, res, next) {
     var query = req.query['filterObj'];
    console.log(query);
      router.loadProductJson(function(data){
        res.json(data);  
    }); 
});


module.exports = router;
