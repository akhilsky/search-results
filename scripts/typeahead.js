(function(EW, window, document) {
    var element_obj = {}
        ,requiredPage = 0
        ,totalePages ='';


  var typeahead =  {
      initVariables :function(){
        element_obj = {
          'pdp_zoom_gallery': EW('#pdp_zoom_gallery'),
        };
        
       },
      init:function(){
		  typeahead.initSearchTypeAhead();
         
      },
      initEvents : function(){
          // EW(document).on('click','#btn_ZoomIn',pdp.productZoomIn);      
      },
     
	initSearchTypeAhead:function(){
		    // Initialize autocomplete with local lookup:
		EW('#autocomplete').devbridgeAutocomplete({
				serviceUrl:"http://localhost:3000/users/search",
				deferRequestBy:300,
				minChars: 1,
				onSelect: function (suggestion) {
					var query = EW("#autocomplete").val();
					window.location = "results.html?q=" + query;
				   // EW('#selection').html('You selected: ' + suggestion.value + ', ' + suggestion.data.category);

				},
			
				showNoSuggestionNotice: true,
				noSuggestionNotice: EW('#search-result-defaults').show(),
				appendTo:EW("#search-result"),
			 //   groupBy: 'cat',

				formatResult: function (suggestion, currentValue) {
					console.log(suggestion);
					return '<span class="'+suggestion.data.category+'">'+EW.Autocomplete.defaults.formatResult(suggestion, currentValue)+'</span>';
			   
				},
		 
				// onSearchStart: function (query) {
				//     console.log(query);
				// }
				// onSearchComplete: function (query, suggestions) {
				//    EW('.result-msg').html('')
				//    var searchResultMessage = '<p>Good news,<span>'+suggestions.length+'</span> results found for "'+query+'"</p>';
				//    EW('.result-msg').html(searchResultMessage);
				// }
				beforeRender: function (container,suggestion) {
					 EW(".autocomplete-suggestion .MEN").html(function(){
						var html= EW(this).html();
						var splitHtml = html.split(" ")
						splitHtml[0] = "<span class='blue-texture'>MEN</span>"
						return splitHtml.join(" ");
					   // var first = text.shift();
						// console.log(text);
					  //return (text.length > 0 ? "<span class='blue-texture'>"+ first + "</span> " : first) + text.join(" ");
					 });
					 EW(".autocomplete-suggestion .WOMEN").html(function(){
					   var html= EW(this).html();
						var splitHtml = html.split(" ")
						splitHtml[0] = "<span class='red-texture'>WOMEN</span>"
						return splitHtml.join(" ");
					 });

				 
				}
			})
	}
  }

   EW(function() {
        typeahead.init();
   }); 

}(jQuery, window, document));


