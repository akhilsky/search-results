﻿(function(EW, window, document) {
    var element_obj = {}
        ,nextPage = 0
        ,totalePages =''
        ,currentPage =''
        ,filterObj ={}
        ,pageSize = 0;


  var search =  {
      initVariables :function(){
        element_obj = {
          'pdp_zoom_gallery': EW('#pdp_zoom_gallery'),
        };
        
       },
      init:function(){
          var searchQuery = unescape(search.getUrlVars()["q"]);
          filterObj.searchQuery = searchQuery;
          search.setPageSize();
          search.renderSearchResult(filterObj);  
          search.initEvents();
  
      },
      initEvents : function(){   
         EW(document).on('change','#facetsMenu input',search.initFilterAjax);      
      },
    
      initLazyLoading:function(){
      //  search.getNextProducts();
      },
      initFilterAjax:function(){
       var selectedFacets = [];
        EW('#facetsMenu input:checked').each(function () {
            selectedFacets.push(EW(this).val());
        });
        filterObj.selectedFacets = selectedFacets;
        search.getNextProducts(filterObj);
        
      },
      renderSearchResult:function(filterObj){
        EW.ajax({
            type: "get",
            url: "http://localhost:3000/users/search-results",
            data: {filterObj},
            cache: false,
            success: function(res){
                search.renderFacets(res.facets);
                search.renderProducts(res.results)
            }
        });
        
      } ,
      getNextProducts:function(filterObj){
        EW('#products .ajax-loader').show()
            EW.ajax({
                type: "get",
                url: "http://localhost:3000/users/search-results",
                data: {filterObj},
                cache: false,
                success: function(res){
                  search.renderFacets(res.facets);
                  search.renderProducts(res.results)
                  EW('#products .ajax-loader').hide()
                  totalePages = res['pagination']['numberOfPages'];
                  currentPage = res['pagination']['currentPage']+1;
                  filterObj.requiredPage = nextPage = currentPage+1;
                }
            });
        
      },
      renderFacets:function(facetsArray){
        if(facetsArray && facetsArray.length){
          EW("#facetsMenu").html("");
          facetsArray.forEach(function(facet,index){
            var facetsHtml = '';
            facetsHtml +=  '<div class="panel panel-default">'
            facetsHtml +=  '<div class="panel-heading" role="tab" id="heading'+index+'">'
            facetsHtml +=  '<h4 class="panel-title">'
            facetsHtml +=  '<a role="button" data-toggle="collapse" data-parent="#facetsMenu" href="#collapse'+index+'" aria-expanded="false" aria-controls="collapseOne">'
            facetsHtml +=   facet['name']
            facetsHtml +=  '</a></h4></div>'
            facetsHtml +=  '<div id="collapse'+index+'" class="panel-collapse collapse" role="tabpanel" aria-labelledby="heading'+index+'">'
             if(facet["values"] && facet["values"].length && facet['visible']==true){
                facetsHtml +=  '<div class="panel-body">'
                facetsHtml +=  '<ul class="nav">'
                facet["values"].forEach(function(value,key){
                    if(facet['multiSelect'] == false){
                      if(value['selected'] == true)
                        facetsHtml += '<div class="radio"><label><input type="radio" checked=true value="'+value['query']['query']['value']+'"  name="'+facet['name']+'">'+value['name']+'</label></div>'
                     if(value['selected'] == false)
                        facetsHtml += '<div class="radio"><label><input type="radio" value="'+value['query']['query']['value']+'"  name="'+facet['name']+'">'+value['name']+'</label></div>'
                    }   
                    if(facet['multiSelect'] == true){
                       if(value['selected'] == true)
                      facetsHtml += '<div class="checkbox"><label><input type="checkbox" class="inputcheckbox" checked=true value="'+value['query']['query']['value']+'">'+value['name']+'</label></div>'
                     if(value['selected'] == false)
                      facetsHtml += '<div class="checkbox"><label><input type="checkbox" class="inputcheckbox" value="'+value['query']['query']['value']+'">'+value['name']+'</label></div>'
                    }
                })
               facetsHtml +=  '</ul></div>'
              }
            facetsHtml +=  '</div> </div>'
            EW("#facetsMenu").append(facetsHtml);
          });    
        }
         
      },
      renderProducts:function(productsArray){
        if(productsArray && productsArray.length){
            productsArray.forEach(function(value,index){
              var productHtml = '';
              productHtml += '<div class="item  col-xs-4 col-lg-4 lazy">'
              productHtml += '<div class="thumbnail">'
              productHtml += '<img class="group list-group-image" src="http://placehold.it/400x250/000/fff" alt="" />'
              productHtml += '<div class="caption">'
              productHtml += '<h4 class="group inner list-group-item-heading">'
              productHtml += 'Product title'
              productHtml += '</h4><p class="group inner list-group-item-text">'
              productHtml += 'Product description... Lorem ipsum dolor sit amet, consectetuer adipiscing elit,</p>'
              productHtml += '<div class="row">'
              productHtml += '<div class="col-xs-12 col-md-6">'
              productHtml += '<p class="lead">'
              productHtml +=  '$21.000'
              productHtml += '</p></div>'
              productHtml += '<div class="col-xs-12 col-md-6">'
              productHtml += '<a class="btn btn-success" href="http://www.jquery2dotnet.com">Add to cart</a>'
              productHtml += '</div></div></div></div></div>'
              EW('#products').append(productHtml);
            });
             
        }
         
      },
      setPageSize:function(){
          if(EW(window).width() < 768){
            filterObj.pageSize = 8
          }
          if(EW(window).width() >= 768){
            filterObj.pageSize = 12
          }
      },
      getUrlVars: function () {
            var vars = [], hash;
            var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for(var i = 0; i < hashes.length; i++)
            {
                hash = hashes[i].split('=');
                vars.push(hash[0]);
                vars[hash[0]] = hash[1];
            }
            return vars;
        },
        resizeWindow:function(){
             filterObj.requiredPage = nextPage = currentPage;
            search.setPageSize();
            console.log(filterObj)
           // search.getNextProducts(filterObj);
        }  
    }

    var TO = false;
    var resizeEvent = 'onorientationchange' in window ? 'orientationchange' : 'resize';
    $(window).bind(resizeEvent, function() {
        TO && clearTimeout(TO);
        TO = setTimeout(search.resizeWindow, 200);
    });
   

   EW(window).scroll(function () {//
      if (EW(window).height() + EW(window).scrollTop() >= EW(document).height()) {
        // if(nextPage <= totalePages)
          search.getNextProducts(filterObj);
      }
    });
   EW(function() {
        search.init();
   }); 

}(jQuery, window, document));


