﻿/*jslint  browser: true, white: true, plusplus: true */
/*global $, countries */

$(function () {
    'use strict';
    $("#checkAll").click(function () {
    $(".check").prop('checked', $(this).prop('checked'));
});

    // Initialize autocomplete with local lookup:
$('#autocomplete').devbridgeAutocomplete({
        serviceUrl:"http://localhost:3000/users/search",
        deferRequestBy:300,
        minChars: 1,
        onSelect: function (suggestion) {
            var query = $("#autocomplete").val();
            window.location = "results.html?q=" + query;
           // $('#selection').html('You selected: ' + suggestion.value + ', ' + suggestion.data.category);

        },
    
        showNoSuggestionNotice: true,
        noSuggestionNotice: 'Sorry, no matching results',
        appendTo:$("#search-result"),
     //   groupBy: 'cat',

        formatResult: function (suggestion, currentValue) {
            return '<span class="'+suggestion.data.category+'">'+$.Autocomplete.defaults.formatResult(suggestion, currentValue)+'</span>';
       
        },
 
        // onSearchStart: function (query) {
        //     console.log(query);
        // }
        // onSearchComplete: function (query, suggestions) {
        //    $('.result-msg').html('')
        //    var searchResultMessage = '<p>Good news,<span>'+suggestions.length+'</span> results found for "'+query+'"</p>';
        //    $('.result-msg').html(searchResultMessage);
        // }
        beforeRender: function (container,suggestion) {
             $(".autocomplete-suggestion .MEN").html(function(){
                var html= $(this).html();
                var splitHtml = html.split(" ")
                splitHtml[0] = "<span class='blue-texture'>MEN</span>"
                return splitHtml.join(" ");
               // var first = text.shift();
                // console.log(text);
              //return (text.length > 0 ? "<span class='blue-texture'>"+ first + "</span> " : first) + text.join(" ");
             });
             $(".autocomplete-suggestion .WOMEN").html(function(){
               var html= $(this).html();
                var splitHtml = html.split(" ")
                splitHtml[0] = "<span class='red-texture'>WOMEN</span>"
                return splitHtml.join(" ");
             });

         
        }
    })
 
 
});